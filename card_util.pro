TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lcurl
SOURCES += main.cpp \
    request.cpp \
    printer.cpp \
    requestfacade.cpp \
    config.cpp \
    helpers.cpp \
    getuserinfo.cpp \
    getcardinfo.cpp \
    sendtransaction.cpp

HEADERS += \
    request.h \
    printer.h \
    requestfacade.h \
    config.h \
    helpers.h \
    getuserinfo.h \
    getcardinfo.h \
    sendtransaction.h

DISTFILES += \
    README.txt \
    card_util.log \
    card_util.cfg

