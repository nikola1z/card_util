#include <string>
#include <iostream>
#include <fstream>
#include "plog/Log.h"
#include "json.hpp"
#include "config.h"

using json = nlohmann::json;

Config::Config(string filename)
{
    ParseCfg(filename);
}

void Config::ParseCfg(string file)
{
    ifstream config_file;
    string config_string;
    config_file.open(file);
    if (!config_file)
    {
        LOG_DEBUG << "Не могу открыть конфигурационный файл";
        exit(0);
    } else
    {
        getline(config_file, config_string, '\0');
        config_file.close();
        auto j = json::parse(config_string);
        login = j["login"];
        password = j["password"];
        url = j["url"];
        LOG_DEBUG << "Конфигурационный файл успешно прочитан";
    }
}

