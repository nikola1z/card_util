#include <string>
#ifndef CONFIG_H
#define CONFIG_H

using namespace std;

class Config
{
public:

    string url;
    string login;
    string password;

    Config(string file);

private:
    void ParseCfg(string file);
};

#endif // CONFIG_H
