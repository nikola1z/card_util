#include <iostream>
#include "getcardinfo.h"
#include "helpers.h"

GetCardInfo::GetCardInfo()
{

}

void GetCardInfo::set_options(map<string, string> parameters)
{
    struct curl_slist *chunk = NULL;
    curl_easy_setopt(curl, CURLOPT_URL, parameters["url"].c_str());
    chunk = curl_slist_append(chunk, "Content-Type: application/json");
    chunk = curl_slist_append(chunk, parameters["authorization"].c_str());

    response = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
}

bool GetCardInfo::validate_parameters(int argc, char *argv[])
{
    if (argc == 3)
    {
        if (is_valid_ean(argv[2]))
        {
            return true;
        } else
        {
            cerr << "Wrong ean" << endl;
            return false;
        }
    } else
    {
        cerr << "Usage: card_util get_card_info [<ean>]" << endl;
        return false;
    }
}
