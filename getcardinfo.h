#ifndef GETCARDINFO_H
#define GETCARDINFO_H
#include "request.h"

class GetCardInfo : public Request
{
public:
    GetCardInfo();
    bool validate_parameters(int argc, char *argv[]);
private:
    void set_options(map<string, string> parameters);
};

#endif // GETCARDINFO_H
