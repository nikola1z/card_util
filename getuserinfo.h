#ifndef GETUSERINFO_H
#define GETUSERINFO_H
#include "request.h"

class GetUserInfo : public Request
{
public:
    GetUserInfo();
    bool validate_parameters(int argc, char *argv[]);
private:
    void set_options(map<string, string> parameters);
};

#endif // GETUSERINFO_H
