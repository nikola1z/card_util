#include <string>

using namespace std;

bool is_digits(const string &str)                   // функция для проверки состоит ли строка из цифр
{
    for(int counter = 0; counter < str.size(); counter++)
    {
        if (!isdigit(str[counter]))
        {
            return false;
        }
    }
    return true;
}


bool is_valid_ean(const string &ean)                // функция для проверки является ли строка ean
{

    int odd {0}, even {0};
    if (ean.size() != 13)
    {
        return false;
    }

    for(int counter = 0; counter < 12; counter++) {
        if (!isdigit(ean[counter]))
        {
            return false;
        } else
        {
            if ((counter+1) % 2 == 1)
            {
                odd += int(ean[counter]) - 48;
            } else
            {
                even += int(ean[counter]) - 48;
            }
        }
    }
    if  ( int(ean[12]) - 48 == 10 - (even * 3 + odd) % 10)
    {
        return true;
    }
    return false;
}
