#ifndef HELPERS
#define HELPERS
#include <string>
using namespace std;

bool is_digits(const string &str);
bool is_valid_ean(const string &ean);
#endif // HELPERS

