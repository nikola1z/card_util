#include <iostream>
#include <requestfacade.h>
#include <config.h>
#include "plog/Log.h"

using namespace std;

int main(int argc, char *argv[])
{
    plog::init(plog::debug, "card_util.log");   //инициализируем логгер

    RequestFacade request_facade;
    request_facade.submit_request(argc, argv);
    return 0;
}

