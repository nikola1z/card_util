#include <iostream>
#include "printer.h"
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

Printer::Printer()
{

}

void Printer::print(int response_code, string response) {
    if (response_code == 200)
    {
        cout << "response status: 0" << endl;
        cout << "response message: " << endl;
        auto json_response = json::parse(response);

        for (json::iterator it = json_response.begin(); it != json_response.end(); ++it)
        {
            cout << it.key() << " : " << it.value() << endl;
        }
    } else
    {
        cout << "response status: 1" << endl;
        cout << "response message: " << response << endl;
    }
}
