#ifndef PRINTER_H
#define PRINTER_H
#include "request.h"

class Printer
{
public:
    Printer();
    void print(int response_code, string response);
};

#endif // PRINTER_H

