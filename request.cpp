#include <iostream>
#include <typeinfo>
#include "request.h"
#include <curl/curl.h>

#include <string>
using namespace std;
Request::Request()
{

}

int Request::writer(char *data, size_t size, size_t nmemb, string *buffer)
{
    int result = 0;
    if (buffer != NULL)
    {
        buffer->append(data, size * nmemb);
        result = size * nmemb;
    }
    return result;
}

void Request::cleanup(void)
{
    curl_easy_cleanup(curl);
}

void Request::perform(void)
{
    result = curl_easy_perform(curl);
    if(result == CURLE_OK)
    {
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
    }
}

void Request::init(void)
{
    curl = curl_easy_init();
}

bool Request::validate_parameters(int argc, char *argv[])
{
    return true;
}

void Request::set_options(map<string, string> parameters)
{

}

void Request::make_request(map<string, string> parameters)
{
    init();
    set_options(parameters);
    perform();
    cleanup();
}

