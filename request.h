#ifndef REQUEST_H
#define REQUEST_H
#include <string>
#include <map>
#include <curl/curl.h>

using namespace std;
class Request
{
public:
    Request();
    int status;
    string response;
    string message;
    CURL *curl;
    CURLcode result;
    string buffer;
    long response_code;
    static int writer(char *data, size_t size, size_t nmemb, string *buffer);
    void make_request(map<string, string> parameters);
    virtual bool validate_parameters(int argc, char *argv[]) =0;
private:
    virtual void set_options(map<string, string> parameters) =0;
    void init(void);
    void perform(void);
    void cleanup(void);
};

#endif // REQUEST_H

