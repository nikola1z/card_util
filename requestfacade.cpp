#include <iostream>
#include "plog/Log.h"
#include "requestfacade.h"
#include "config.h"
#include "helpers.h"
#include "getcardinfo.h"
#include "getuserinfo.h"
#include "sendtransaction.h"
#include "printer.h"
using namespace std;

RequestFacade::RequestFacade()
{

}

void RequestFacade::submit_request(int argc, char *argv[])
{
    map<string, string> parameters;
    Printer my_printer;

    Config my_config("card_util.cfg");                                                              // Получаем данные из файла конфигурации

    if (argv[1])
    {
        string command = argv[1];
        if (command == "get_card_info")
        {
            GetCardInfo get_card_info_request;
            if (get_card_info_request.validate_parameters(argc, argv))
            {
                parameters["url"] = my_config.url + argv[2];
                get_card_info_request.make_request(parameters);                                      // Выполняем запрос

                my_printer.print(get_card_info_request.response_code, get_card_info_request.buffer); // Выводим результаты запроса на экран
            }
        } else if (command == "get_user_info")
        {
            GetUserInfo get_user_info_request;
            if (get_user_info_request.validate_parameters(argc, argv))
            {
                parameters["url"] = my_config.url + argv[2] + "/holder";

                get_user_info_request.make_request(parameters);

                my_printer.print(get_user_info_request.response_code, get_user_info_request.buffer);
            }
        } else if (command == "send_transaction")
        {
            SendTransaction send_transaction_request;
            if (send_transaction_request.validate_parameters(argc, argv))
            {
                string postfields("type=spent&sum="), card_number {argv[2]}, value {argv[3]};

                parameters["url"] = my_config.url + card_number + "/transactions";
                parameters["postfields"] = postfields + value;

                send_transaction_request.make_request(parameters);

                my_printer.print(send_transaction_request.response_code, send_transaction_request.buffer);
            }
        } else
        {
            cout << "Unknown command " << argv[1] << endl;
        }
    } else
    {
        cerr << "No command provided" << endl;
    }

}
