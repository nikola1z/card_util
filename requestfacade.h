#ifndef REQUESTFACADE_H
#define REQUESTFACADE_H

#include "request.h"

class RequestFacade
{
public:
    RequestFacade();
    void submit_request(int argc, char *argv[]);
};

#endif // REQUESTFACADE_H
