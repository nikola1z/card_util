#include <iostream>
#include "sendtransaction.h"
#include "helpers.h"

SendTransaction::SendTransaction()
{

}

void SendTransaction::set_options(map<string, string> parameters)
{
    struct curl_slist *chunk = NULL;
    curl_easy_setopt(curl, CURLOPT_URL, parameters["url"].c_str());
    chunk = curl_slist_append(chunk, "Content-Type: application/json");
    chunk = curl_slist_append(chunk, parameters["authorization"].c_str());

    response = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, parameters["postfields"].c_str());
}

bool SendTransaction::validate_parameters(int argc, char *argv[])
{
    if (argc == 4)
    {

        if (!is_digits(argv[3]))
        {
            cerr << "Wrong transaction value" << endl;
            return false;
        }
        if (is_valid_ean(argv[2]))
        {
            return true;
        } else
        {
            cerr << "Wrong ean" << endl;
            return false;
        }

    } else
    {
        cerr << "Usage: card_util send_transaction [<ean>] [<value>]" << endl;
        return false;
    }
}
