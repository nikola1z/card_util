#ifndef SENDTRANSACTION_H
#define SENDTRANSACTION_H
#include "request.h"

class SendTransaction : public Request
{
public:
    SendTransaction();
    bool validate_parameters(int argc, char *argv[]);
private:
    void set_options(map<string, string> parameters);
};

#endif // SENDTRANSACTION_H
